$(document).ready(function(){

  $('#promo-slider').owlCarousel({
    items: 1,
    dots: false,
    nav: true,
    navText: ['<svg width="10px" height="17px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>', '<svg width="10px" height="17px" viewBox="0 0 50 80" xml:space="preserve"><polyline fill="none" stroke="#525f7f" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="35.63,75.8 0.375,38.087 35.63,0.375 "/></svg>'],
    autoplay: true,
    autoplayTimeout: 3000,
    loop: true,
    smartSpeed: 750,
    margin: 2
  });

  $('#clients-slider').owlCarousel({
    dots: false,
    autoplay: true,
    autoplayTimeout: 3000,
    loop: true,
    smartSpeed: 500,
    responsive: {
      0: {
        items: 2,
        margin: 15
      },
      448: {
        items: 4,
        margin: 15
      },
      768: {
        items: 5,
        margin: 30
      },
      992: {
        items: 6,
        margin: 30
      }
    }
  });

});